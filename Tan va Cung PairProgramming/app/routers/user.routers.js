module.exports = function(app) {

    const users = require('../controllers/user.controller.js');

    // Create a new user
    app.post('/users', users.create);

    // Retrieve all user
    app.get('/users', users.findAll);

    // Check login
    app.get('/users/login', users.login);


    // Delete a user with Id
    app.delete('/users/:id', users.delete);


    // Forgot password
    app.put('/users/forgot', users.forgot);

    // Forgot password active
    app.get('/users/forgot/active', users.forgotActive);

    // Update password
    app.put('/users/update/:id', users.update);




}