const fs = require('fs');

exports.readData = function readData(fileName) {
    return data = JSON.parse(fs.readFileSync(fileName, "utf-8"));
}

exports.writeData = function writeData(fileName, content){
    fs.writeFileSync(fileName, content, "utf-8");
}