// import library
const md5 = require("blueimp-md5"); // note
// import file
const readData = require('./readData.controller');
// khai bao
const root = 'http://localhost:3000';
const fileName = process.cwd() + '/data.json'; //notee

const users = readData.readData(fileName);

exports.findAll = function (req, res) {
	res.json(users);
};

exports.create = function (req, res) {
	let body = req.body;
	let newuser = {		// note
		id: body.id,
		username: body.username,
		password: body.password,
		email: body.email,
		state: body.state,
		role: body.role
	};

	if (newuser.password === body.confirmpassword) {
		users["user" + body.id] = newuser;
		readData.writeData(fileName, JSON.stringify(users, null, 4));
		res.json(newuser);
	} else {
		throw new Error(`Password not match!`); // note
	}
};
//note Object.key
exports.login = function (req, res) {
	// const query = req.query;
	const { query } = req;
	let tempUsers = Object.keys(users);
	let userLogin = tempUsers.find((user) => {
		return ((query.email == users[user].email) && (query.password == users[user].password));
	});
	if (userLogin) {
		res.json(users[userLogin]); // note
	} else {
		// throw new Error(`Login failed!`); // note
		res.json({});
	}
};


exports.delete = function (req, res) {
	let deleteuser = users["user" + req.params.id];
	delete users["user" + req.params.id];
	res.json(deleteuser);
	readData.writeData(fileName, JSON.stringify(users, null, 4));
};

exports.forgot = (req, res) => {
	const { body } = req;
	let token;
	let id;

	let tempUsers = Object.keys(users);
	let userForgot = tempUsers.find((user) => {
		return (body.email === users[user].email);
	});
	token = md5(body.email);
	id = users[userForgot].id;

	if (token) {
		res.end(`Please check your email
Email: ${root}/users/forgot/active?token=${token}&id=${id}`);
	} else {
		throw new Error(`Email does not exist!`); // note
	}
}

exports.forgotActive = function (req, res) {
	let query = req.query;

	let tempUsers = Object.keys(users);
	let userActive = tempUsers.find((user) => {
		return ((query.id == users[user].id) && (query.token == md5(users[user].email)));
	});
	users[userActive].password = 'Conheocon@97';
	let userForgot = users[userActive];

	if (userForgot) {
		readData.writeData(fileName, JSON.stringify(users, null, 4));
		res.end(`Your Password: ${users[userActive].password} \n 
Change password: ${root}/users/update/${userForgot.id}`);
	} else {
		throw new Error(`Token has expired or does not exist!`); // note
	}
}

exports.update = function (req, res) {
	let id_edit = req.params.id;
	let { body } = req;
	let tempUsers = Object.keys(users);

	if (users["user" + id_edit] != null) {
		let userActive = tempUsers.find((user) => {
			return (id_edit == users[user].id);
		});

		users[userActive].name = body.name;
		users[userActive].password = body.password;
		let userUpdate = users[userActive];

		readData.writeData(fileName, JSON.stringify(users, null, 4));
		res.json(userUpdate);
	} else {
		throw new Error(`Don't Exist user!`); // note
	}
}
