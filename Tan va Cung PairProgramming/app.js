const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.json())

require('./app/routers/user.routers.js')(app);

// Create a Server
const server = app.listen(3000, function () {

  const host = server.address().address
  const port = server.address().port

  console.log("App listening at http://%s:%s", host, port)

})