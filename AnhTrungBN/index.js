var express = require('express');
var bodyParser = require('body-parser')
var md5 = require("blueimp-md5");

var app = express();
var fs = require("fs");

// create application/json parser
var jsonParser = bodyParser.json();
app.use(bodyParser.json({type: 'application/*+json'}));

var base_url = "http://localhost:8081";

// List user
app.get('/user/list', function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        res.end(data);
    });
});

// get user (login)
app.get('/user/login', function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse(data);
        var user;
        const query = req.query;
        users.forEach(function (u) {
            if ((query.email == u.email) && (query.password == u.password)) {
                user = u;
            }
        });
        if (user) {
            res.end(JSON.stringify(user));
        } else {
            res.end(JSON.stringify("Mật khẩu hoặc password không chính xác"));
        }
    });
});

// Add user
app.post('/user/add', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse(data);
        const body = req.body;
        var user = {};
        var id_new = 0;
        users.forEach(function (u, index) {
            if (id_new < u.id) {
                id_new = u.id;
            }
        });
        user.name = body.name;
        user.email = body.email;
        user.password = body.password;
        user.repassword = body.repassword;
        user.id = id_new + 1;
        if (user.email != '' && user.password != '') {
            if (user.password != user.repassword) {
                res.end("Mat khau nhap lai ko giong nhau");
            } else {
                users.push(user);
                fs.writeFileSync(__dirname + "/" + "users.json", JSON.stringify(users));
                res.end(JSON.stringify(user));
            }

        } else {
            res.end("Email và password không được để trống");
        }
    });
});

// Update user
app.put('/user/update/:id', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse(data);
        const id_edit = req.params.id;
        const body = req.body;
        var user;
        users.forEach(function (u, index) {
            if (id_edit == u.id) {
                users[index].name = body.name;
                users[index].password = body.password;
                user = users[index];
            }
        });
        fs.writeFileSync(__dirname + "/" + "users.json", JSON.stringify(users));
        res.end(JSON.stringify(user));
    });
});

// Forgot password
app.put('/user/forgot-password', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse(data);
        const body = req.body;
        var token;
        var id;
        users.forEach(function (u, index) {
            if (body.email == u.email) {
                token = md5(body.email);
                id = u.id;
            }
        });
        if (token) {
            res.end(JSON.stringify(`Vui lòng kiểm tra email để kích hoạt lại password \n
            Email: ${base_url}/user/forgot-password/active?token=${token}&id=${id}`));
        } else {
            res.end(JSON.stringify(`Email không tồn tại`));
        }
    });
});

// Forgot password active
app.get('/user/forgot-password/active', jsonParser, function (req, res) {
    fs.readFile(__dirname + "/" + "users.json", 'utf8', function (err, data) {
        var users = JSON.parse(data);
        const query = req.query;
        var user;
        users.forEach(function (u, index) {
            if ((query.id == u.id) && (query.token == md5(u.email))) {
                users[index].password = '12345!@#';
                user = users[index];
            }
        });
        if (user) {
            fs.writeFileSync(__dirname + "/" + "users.json", JSON.stringify(users));
            res.end(`Password của bạn đã được thay đổi về mặc định '12345!@#' \n 
                Bạn có thể vào link sau để đổi mật khẩu ${base_url}/user/update/${user.id}
            `);
        } else {
            res.end("Token đã hết hạn hoặc ko tồn tại");
        }
    });
});


var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});
